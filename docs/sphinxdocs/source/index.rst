Documentation homepage
================================

.. include:: myst_include_readme.md
   :parser: myst_parser.sphinx_

-------------------

More documentation:

.. toctree::
   :maxdepth: 1

   self
   tutorials/index
   templates/facile-rs_template_gitlab
   templates/facile-rs_template_github
   apidocs/index

* :ref:`Index <genindex>`
* :ref:`Module Index <modindex>`
